
public class Test02 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello World!!");
		System.out.printf("Hello %s!%n", "lovely World"); 
		//Der Unterschied zwischen print() und println() ist, dass ln= line(Zeile) bedeutet//
		//Bei printls() wird die neue Ausgabe in eine neue Zeile erfolgen//
		//Bei print() wird auf der aktuellen Zeile weitergeschrieben//
		
		System.out.println("       *");
		System.out.println("      ***");
		System.out.println("     *****");
		System.out.println("    *******");
		System.out.println("   *********");
		System.out.println("  ***********");
		System.out.println(" *************");
		System.out.println("      ***");
		System.out.println("      ***");
		
		
	}

}
