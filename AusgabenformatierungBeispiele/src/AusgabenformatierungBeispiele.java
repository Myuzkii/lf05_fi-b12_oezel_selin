
public class AusgabenformatierungBeispiele {
	public static void main(String[] args) {
		
		//Ganzzahl
		System.out.printf("|%+-10d%+10d|", 123456, -123456);
		
		//Kommazahl
		System.out.printf("|%+-10.2f|",12.123456789);
		
		//Senkrechte Striche müssen nicht existieren(gibt nur Übersicht)
		//Zahl nach % machts linksbündig, - macht Rechtsbündig
		//Zahl nach dem . gibt die x Stelle an
		
		//Zeichenkette
		System.out.printf("|%-10.3s|","Max Mustermann");
		
		
		System.out.printf("Name:%-10s  Alter:%-8d  Gewicht:%-10.2f%n" , "Max", 18, 80.50);
	}	//Nächste Zeile wechseln: %n oder Backshlashn
}
