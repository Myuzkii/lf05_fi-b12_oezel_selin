import java.util.Arrays;

public class ArrayBeispiel {

	public static void main(String[] args) {
		//Beispiel Aufgabe an der Tafel 
		/*int[] nmr = new int[24];
		
		nmr[2] = 10;
		
		for(int i = 1; i <= 24; i++) {
			
			nmr[i] = i;
		}
		
		System.out.println(nmr[0]);
		
		for (int j = 1; j <= 24; j++) {
			System.out.println(nmr[j]);
		}*/
		
		
		//A5.2 Aufgabe 3 und 4
		
		//Aufgabe 3
		int[] palindrom = {1, 2, 3, 4, 5};
		
		for (int i = palindrom.length-1; i > -1; i--) {
			System.out.println(palindrom[i]);
		}
		
		
		//Aufgabe 4
		int[] lotto = {3, 7, 12, 18, 37, 42}; 
		System.out.println(Arrays.toString(lotto));
		System.out.println("Die Zahl " + lotto[2] + " ist in der Ziehung enthalten.");
	}
}