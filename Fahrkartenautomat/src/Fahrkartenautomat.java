﻿//import java.security.PublicKey;
import java.util.Scanner;

class Fahrkartenautomat
{
	public static void main(String[] args)
    {	
		//while ( true ) // A4.5 2.
		//{
			 double zuZahlenderBetrag 	= fahrkartenBestellungErfassen();
		     double rueckgeld 			= fahrkartenBezahlen ( zuZahlenderBetrag );
		     
		     fahrkartenAusgeben ();
		     rueckgeldAusgeben  ( rueckgeld );
		//}
    }
	
	public static double fahrkartenBestellungErfassen() 
	{
		Scanner tastatur = new Scanner(System.in);
		
		final double einzelfahrscheinAB = 2.9;
		final double tageskarteAB		= 8.6;
		final double kleingruppenAB 	= 25.5;
		double einzelpreis 				= 0;
		
		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
		System.out.println("  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
		System.out.println("  Tageskarte Regeltarif AB [8,60 EUR] (2)");
		System.out.println("  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
		
		int tarif = 0;
		
		while ( tarif < 1 || tarif > 3 ) 
		{
			System.out.print("Ihre Wahl: ");
			tarif = tastatur.nextInt();
			System.out.println();
			if ( tarif < 1 || tarif > 3 ) { System.out.println(" >>Falsche Eingabe<<"); } 
		}
		
		switch (tarif)
		{
		case 1:
			einzelpreis = einzelfahrscheinAB;
			break;
		case 2:
			einzelpreis = tageskarteAB;
			break;
		case 3:
			einzelpreis = kleingruppenAB;
			break;	
		}
		
	    System.out.print("Anzahl der Tickets: ");
	    int anzahlTickets 	 = tastatur.nextInt();
	    
	    if ( anzahlTickets > 10 || anzahlTickets < 1 )
	    {
	    	anzahlTickets = 1;
	    	System.out.println( "Ungültige Eingabe!" );
	    	System.out.println( "Die Anzahl der tickets wurde auf eins gesetzt." );
	    }
	    
	    double zuZahlenderBetrag =  einzelpreis * anzahlTickets;
		return zuZahlenderBetrag;
	}
	
	public static double fahrkartenBezahlen ( double zuZahlenderBetrag )
	{
		Scanner tastatur = new Scanner(System.in);
		
		double eingezahlterGesamtbetrag = 0; 
		double eingeworfeneMünze;
		
	    while ( eingezahlterGesamtbetrag < zuZahlenderBetrag )
	       {
	    	   System.out.printf("Noch zu zahlen: " + "%.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag) );
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze 		 = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	       }
	    
	   
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}
	
	public static void fahrkartenAusgeben () 
	{
		System.out.println("\nFahrschein wird ausgegeben"); 
      
	    for (int i = 0; i < 8; i++)
	    {
	       System.out.print("=");
	       warte(50);
	    }
	    System.out.println("\n\n");
	}
	
	public static void rueckgeldAusgeben (double rueckgeld) 
	{
		if(rueckgeld > 0.0)
		{
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO \n", rueckgeld );
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while(rueckgeld >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
		        rueckgeld -= 2.0;
	        }
	        while(rueckgeld >= 1.0) // 1 EURO-Münzen
            {
	        	muenzeAusgeben(1, "EURO");
	        	rueckgeld -= 1.0;
            }
	        while(rueckgeld >= 0.5) // 50 CENT-Münzen
	        {
	        	muenzeAusgeben(50, "Cent");
	        	rueckgeld -= 0.5;
	        }
	           while(rueckgeld >= 0.2) // 20 CENT-Münzen
	           {
	        	  muenzeAusgeben(20, "Cent");
	 	          rueckgeld -= 0.2;
	           }
	           while(rueckgeld >= 0.1) // 10 CENT-Münzen
	           {
	        	  muenzeAusgeben(10, "Cent");
	        	  rueckgeld -= 0.1;
	           }
	           while(rueckgeld >= 0.05)// 5 CENT-Münzen
	           {
	        	  muenzeAusgeben(5, "Cent");
	 	          rueckgeld -= 0.05;
	           }
	       }
	    	  System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt."); 
	    	  warte(1000);
	    	  System.out.println("\n\n");
	}
	
	static void warte ( int millisekunde ) 
	{
		try 
		{
			Thread.sleep( millisekunde );
		} 
		catch (InterruptedException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
	static void muenzeAusgeben(int betrag, String einheit)
	{
		System.out.println ( betrag + " " + einheit );
	}
}


/*
	5. Für die Variable Tickets habe ich den Datentyp Integer gewählt, da man nur ganzzahlige en von Tickets haben kann
	6. Bei der Berechnung von  * einzelpreis wird der Integer  mit dem Double einzelreis multipliziert. 
		Dabei wird  automatisch von Java in den Datentyp Double konvertiert


*/