import java.util.Scanner;

public class Schleifen_Aufgaben {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Bitte die größte Zahl der Reihe angeben: ");
		int n = scanner.nextInt();

		System.out.println("Die Summe der Folge 1 - " + n + " ist gleich: " + sum(n));

		// Aufgabe 4: Folgen

//a)
		// for (int i = 99; i >= 9; i -= 3) {

		// System.out.println(i + "");
		// }
//c)
		// for (int i = 2; i <= 102; i += 4) {
		// System.out.println(i + "");
		// }
//e)
		// for (int i = 2; i <= 32768; i *= 2) {
		// System.out.println(i + "");
		// }
	}

	// Aufgabe 2: Summe

//a 
	public static int sum(int n) {
		int sum = 0;

		for (int i = 1; i <= n; i++) {
			sum += i;
		}
		return sum;

	}
//b
	
}
